﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deber1Resulto
{
    class Legumbre: Producto
    {

        // Atributos
        private double glucidos;


        /*
         2. En las legunbres el mensaje debe ser el siguiente.
        "La legumbre XX tiene X cantidad de Glusidos, ademas esta compuesta de X proteina y X Vitaminas"
        "La legumbre XX --x nombre del producto
        Glusidos --> glucidos
        proteina --> Padre
        Vitaminas--> Padre
         */

        // Metodos 

        // Sobreescitura;

        public override String obtenerDescripcionProducto()
        {
            return "La legumbre: " + Nombre + " tiene " + glucidos + " g de glucidos, ademas esta compueta de " + Proteina + "g de proteina " 
                +  " y " + Vitamina + " g vitaminas";
        }


        public override string devolverNombre()
        {
            return "";
        }

        // get and set

        public double Glucidos {
            get { return glucidos; }
            set { glucidos = value; }
        }
    }
}
